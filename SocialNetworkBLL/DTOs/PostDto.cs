﻿using System;

namespace SocialNetworkBLL.DTOs
{
    /// <summary>
    /// DTO for the post entity.
    /// </summary>
    public class PostDto
    {
        /// <summary>
        /// Id of the user post belongs to.
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// Text of the post.
        /// </summary>
        public string Text { get; set; }
        /// <summary>
        /// Date and time created.
        /// </summary>
        public DateTime CreatedOn { get; set; }
    }
}
