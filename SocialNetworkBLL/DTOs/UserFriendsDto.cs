﻿namespace SocialNetworkBLL.DTOs
{
    public class UserFriendsDto: BaseDto
    {
        /// <summary>
        /// Id of the user whos friends are.
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// Id of the user's friend.
        /// </summary>
        public int FriendId { get; set; }
    }
}
