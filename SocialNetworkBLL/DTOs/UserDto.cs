﻿using System;
using System.Collections.Generic;

namespace SocialNetworkBLL.DTOs
{
    /// <summary>
    /// Dto to be user for user.
    /// </summary>
    public class UserDto : BaseDto
    {
        /// <summary>
        /// User name.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// User email.
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// User password.
        /// </summary>
        public string Password { get; set; }
        /// <summary>
        /// Date and time created.
        /// </summary>
        public DateTime CreatedOn { get; set; }
        /// <summary>
        /// User friends list.
        /// </summary>
        public IEnumerable<UserDto> Friends { get; set; }
        /// <summary>
        /// User's posts list.
        /// </summary>
        public IEnumerable<PostDto> Posts { get; set; }
        /// <summary>
        /// Id of the user role.
        /// </summary>
        public int RoleId { get; set; }
        /// <summary>
        /// Role of the user.
        /// </summary>
        public RoleDto Role { get; set; }
    }
}
