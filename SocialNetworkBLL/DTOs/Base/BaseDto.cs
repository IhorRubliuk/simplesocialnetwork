﻿namespace SocialNetworkBLL.DTOs
{
    /// <summary>
    /// Class for base DTOs.
    /// </summary>
    public class BaseDto
    {
        /// <summary>
        /// Unique identifier of the DTO.
        /// </summary>
        public int Id { get; set; }
    }
}
