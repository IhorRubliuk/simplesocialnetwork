﻿using System;

namespace SocialNetworkBLL.DTOs
{
    public class PictureDto : BaseDto
    {
        /// <summary>
        /// Path to the picture stored in file system.
        /// </summary>
        public string Path { get; set; }
        /// <summary>
        /// Date and time created.
        /// </summary>
        public DateTime CreatedOn { get; set; }
    }
}
