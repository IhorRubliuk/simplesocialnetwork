﻿using System;

namespace SocialNetworkBLL.DTOs
{
    /// <summary>
    /// DTO for the message entity.
    /// </summary>
    public class MessageDto : BaseDto
    {
        /// <summary>
        /// Id of the user message wa sent from.
        /// </summary>
        public int FromId { get; set; }
        /// <summary>
        /// Id of the user message was sent to.
        /// </summary>
        public int ToId { get; set; }
        /// <summary>
        /// Text message that was sent.
        /// </summary>
        public string Text { get; set; }
        /// <summary>
        /// Date and time created.
        /// </summary>
        public DateTime CreatedOn { get; set; }
    }
}
