﻿using System.Collections.Generic;

namespace SocialNetworkBLL.DTOs
{
    /// <summary>
    /// DTO for user role.
    /// </summary>
    public class RoleDto : BaseDto
    {
        /// <summary>
        /// Name of the user role.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Users that belong to this role.
        /// </summary>
        public IEnumerable<UserDto> Users { get; set; }
    }
}
