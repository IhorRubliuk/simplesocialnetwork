﻿using System;

namespace SocialNetworkBLL.DTOs
{
    /// <summary>
    /// DTO for exception details entity.
    /// </summary>
    public class ExceptionDetailDto : BaseDto
    {
        /// <summary>
        /// Message of the exception
        /// </summary>
        public string ExceptionMessage { get; set; }
        /// <summary>
        /// Name of the controller where exception occurred
        /// </summary>
        public string ControllerName { get; set; }
        /// <summary>
        /// Name of the action where exception occurred.
        /// </summary>
        public string ActionName { get; set; }
        /// <summary>
        /// Stack trace of the exception.
        /// </summary>
        public string StackTrace { get; set; }
        /// <summary>
        /// Date and time of the exception.
        /// </summary>
        public DateTime CreatedOn { get; set; }
    }
}
