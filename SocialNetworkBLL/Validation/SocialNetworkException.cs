﻿using System;
using System.Runtime.Serialization;

namespace SocialNetworkBLL.Validation
{
    [Serializable]
    public class SocialNetworkException : Exception
    {
        public SocialNetworkException() { }
        public SocialNetworkException(string message) : base(message) { }
        public SocialNetworkException(string message, Exception innerException) : base(message, innerException) { }
        protected SocialNetworkException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
            base.GetObjectData(info, context);
        }
    }
}
