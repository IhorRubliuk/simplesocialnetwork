﻿using Mapster;
using SocialNetworkBLL.DTOs;
using SocialNetworkBLL.ExtensionMethods;
using SocialNetworkBLL.Interfaces;
using SocialNetworkDAL.Entities;
using SocialNetworkDAL.ExtensionMethods;
using SocialNetworkDAL.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialNetworkBLL.Services
{
    public class PostService : IPostService
    {
        /// <summary>
        /// Field for unit of work with access to contexts and repositories.
        /// </summary>
        private IUnitOfWork Database { get; set; }
        /// <summary>
        /// Constructor to init service with UoW.
        /// </summary>
        /// <param name="uow">Inistance of unit of work to initialize service with.</param>
        public PostService(IUnitOfWork uow)
        {
            Database = uow;
        }
        /// <summary>
        /// Method to add post to db.
        /// </summary>
        /// <param name="dto">DTO that contains data for the post to be added.</param>
        /// <returns>Task after code execution.</returns>
        public async Task AddAsync(PostDto dto)
        {
            dto.CheckEntity();
            dto.ValidateProperties();
            var entity = dto.Adapt<Post>();
            await Database.PostRepository.AddAsync(entity);
            dto.UserId = entity.UserId;
            await Database.SaveAsync();
        }
        /// <summary>
        /// Method to remove entity from db.
        /// </summary>
        /// <param name="dto">DTO to match real entity in db.</param>
        public void Delete(PostDto dto)
        {
            dto.CheckEntity();
            dto.UserId.ValidateId();
            Database.PostRepository.Delete(dto.Adapt<Post>());
            Database.Save();
        }
        /// <summary>
        /// Method to delete entity from db by it's id.
        /// </summary>
        /// <param name="id">Unique identifier of the entity to be deleted.</param>
        /// <returns>Task after code execution.</returns>
        public async Task DeleteByIdAsync(int id)
        {
            id.ValidateId();
            await Database.PostRepository.DeleteByIdAsync(id);
            await Database.SaveAsync();
        }
        /// <summary>
        /// Method to get all posts from db.
        /// </summary>
        /// <returns>IEnumerable<PostDto> enumerable of posts.</returns>
        public IEnumerable<PostDto> GetAll()
        {
            var result = Database.PostRepository.GetAll()
                .AsEnumerable()
                .Select(x => x.Adapt<PostDto>());
            return result;
        }
        /// <summary>
        /// Method to get post by it's id.
        /// </summary>
        /// <param name="id">Unique identifier of the post to get.</param>
        /// <returns>Post instance if found or null.</returns>
        public async Task<PostDto> GetByIdAsync(int id)
        {
            id.ValidateId();
            var post = await Database.PostRepository.GetByIdAsync(id);
            var result = post.Adapt<PostDto>();
            return result;
        }
        /// <summary>
        /// Updates post instance in db.
        /// </summary>
        /// <param name="dto">DTO to match real entity in db.</param>
        public void Update(PostDto dto)
        {
            dto.CheckEntity();
            dto.ValidateProperties();
            Database.PostRepository.Update(dto.Adapt<Post>());
            Database.Save();
        }
    }
}
