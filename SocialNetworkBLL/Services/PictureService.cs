﻿using Mapster;
using SocialNetworkBLL.DTOs;
using SocialNetworkBLL.ExtensionMethods;
using SocialNetworkBLL.Interfaces;
using SocialNetworkDAL.Entities;
using SocialNetworkDAL.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialNetworkBLL.Services
{
    public class PictureService : IPictureService
    {
        /// <summary>
        /// Field for unit of work with access to contexts and repositories.
        /// </summary>
        private IUnitOfWork Database { get; set; }
        /// <summary>
        /// Constructor to init service with UoW.
        /// </summary>
        /// <param name="uow">Inistance of unit of work to initialize service with.</param>
        public PictureService(IUnitOfWork uow)
        {
            Database = uow;
        }
        /// <summary>
        /// Method to add picture to db.
        /// </summary>
        /// <param name="dto">DTO that contains data for the picture to be added.</param>
        /// <returns>Task after code execution.</returns>
        public async Task AddAsync(PictureDto dto)
        {
            dto.CheckEntity();
            dto.ValidateProperties();
            var entity = dto.Adapt<Picture>();
            await Database.PictureRepository.AddAsync(entity);
            dto.Id = entity.Id;
            await Database.SaveAsync();
        }
        /// <summary>
        /// Method to remove entity from db.
        /// </summary>
        /// <param name="dto">DTO to match real entity in db.</param>
        public void Delete(PictureDto dto)
        {
            dto.CheckEntity();
            dto.Id.ValidateId();
            Database.PictureRepository.Delete(dto.Adapt<Picture>());
            Database.Save();
        }
        /// <summary>
        /// Method to delete entity from db by it's id.
        /// </summary>
        /// <param name="id">Unique identifier of the entity to be deleted.</param>
        /// <returns>Task after code execution.</returns>
        public async Task DeleteByIdAsync(int id)
        {
            id.ValidateId();
            await Database.PictureRepository.DeleteByIdAsync(id);
            await Database.SaveAsync();
        }
        /// <summary>
        /// Method to get all pictures from db.
        /// </summary>
        /// <returns>IEnumerable<PictureDto> enumerable of pictures.</returns>
        public IEnumerable<PictureDto> GetAll()
        {
            var result = Database.PictureRepository.GetAll()
                .AsEnumerable()
                .Select(x => x.Adapt<PictureDto>());
            return result;
        }
        /// <summary>
        /// Method to get picture by it's id.
        /// </summary>
        /// <param name="id">Unique identifier of the picture to get.</param>
        /// <returns>Picture instance if found or null.</returns>
        public async Task<PictureDto> GetByIdAsync(int id)
        {
            id.ValidateId();
            var picture = await Database.PictureRepository.GetByIdAsync(id);
            var result = picture.Adapt<PictureDto>();
            return result;
        }
        /// <summary>
        /// Updates picture instance in db.
        /// </summary>
        /// <param name="dto">DTO to match real entity in db.</param>
        public void Update(PictureDto dto)
        {
            dto.CheckEntity();
            dto.ValidateProperties();
            Database.PictureRepository.Update(dto.Adapt<Picture>());
            Database.Save();
        }
    }
}
