﻿using Mapster;
using SocialNetworkBLL.DTOs;
using SocialNetworkBLL.Interfaces;
using SocialNetworkDAL.Entities;
using SocialNetworkDAL.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialNetworkBLL.Services
{
    /// <summary>
    /// 
    /// </summary>
    public class ExceptionDetailService : IExceptionDetailService
    {
        /// <summary>
        /// Database for the service.
        /// </summary>
        IUnitOfWork Database { get; set; }
        /// <summary>
        /// Constructor for the service.
        /// </summary>
        /// <param name="unitOfWork">Instance of the UoW to work with db context.</param>
        public ExceptionDetailService(IUnitOfWork unitOfWork) => Database = unitOfWork;
        /// <summary>
        /// Method to add exception detail to db.
        /// </summary>
        /// <param name="exceptionDetailDto">Dto to add to the db.</param>
        public void Add(ExceptionDetailDto exceptionDetailDto)
        {
            Database.ExceptionDetailRepository.Add(exceptionDetailDto.Adapt<ExceptionDetail>());
            Database.Save();
        }
        /// <summary>
        /// Method to remove exception details from db.
        /// </summary>
        /// <param name="exceptionDetailDto">Entity to remove from db.</param>
        public void Remove(ExceptionDetailDto exceptionDetailDto)
        {
            Database.ExceptionDetailRepository.Remove(exceptionDetailDto.Adapt<ExceptionDetail>());
            Database.Save();
        }
        /// <summary>
        /// Method to get all logs.
        /// </summary>
        /// <returns>List of logs.</returns>
        public Task<IEnumerable<ExceptionDetailDto>> GetAll()
        {
            var result = Task.Run(() =>
            {
                var list = Database.ExceptionDetailRepository.GetAll();
                return list.Select(x => x.Adapt<ExceptionDetailDto>());
            });
            return result;
        }
    }
}
