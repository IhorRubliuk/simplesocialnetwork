﻿using Mapster;
using SocialNetworkBLL.DTOs;
using SocialNetworkBLL.ExtensionMethods;
using SocialNetworkBLL.Interfaces;
using SocialNetworkDAL.Entities;
using SocialNetworkDAL.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialNetworkBLL.Services
{
    /// <summary>
    /// Service for user.
    /// </summary>
    public class UserService : IUserService
    {
        /// <summary>
        /// Field for unit of work with access to contexts and repositories.
        /// </summary>
        private IUnitOfWork Database { get; set; }
        /// <summary>
        /// Constructor to init service with UoW.
        /// </summary>
        /// <param name="uow">Inistance of unit of work to initialize service with.</param>
        public UserService(IUnitOfWork uow)
        {
            Database = uow;
        }
        /// <summary>
        /// Method to add user to db.
        /// </summary>
        /// <param name="dto">DTO that contains data for the user to be added.</param>
        /// <returns>Task after code execution.</returns>
        public async Task AddAsync(UserDto dto)
        {
            dto.CheckEntity();
            dto.ValidateProperties();
            var entity = dto.Adapt<User>();
            await Database.UserRepository.AddAsync(entity);
            dto.Id = entity.Id;
            await Database.SaveAsync();
        }
        /// <summary>
        /// Method to remove entity from db.
        /// </summary>
        /// <param name="dto">DTO to match real entity in db.</param>
        public void Delete(UserDto dto)
        {
            dto.CheckEntity();
            dto.Id.ValidateId();
            Database.UserRepository.Delete(dto.Adapt<User>());
            Database.Save();
        }
        /// <summary>
        /// Method to delete entity from db by it's id.
        /// </summary>
        /// <param name="id">Unique identifier of the entity to be deleted.</param>
        /// <returns>Task after code execution.</returns>
        public async Task DeleteByIdAsync(int id)
        {
            id.ValidateId();
            await Database.UserRepository.DeleteByIdAsync(id);
            await Database.SaveAsync();
        }
        /// <summary>
        /// Method to get all users from db.
        /// </summary>
        /// <returns>IEnumerable<UserDto> enumerable of users.</returns>
        public IEnumerable<UserDto> GetAll()
        {
            var result = Database.UserRepository.GetAll()
                .AsEnumerable()
                .Select(x => x.Adapt<UserDto>());
            return result;
        }
        /// <summary>
        /// Method to get all users with all navigation properties included.
        /// </summary>
        /// <returns>IQueryable of users.</returns>
        public IEnumerable<UserDto> GetAllWithDetails()
        {
            var result = Database.UserRepository.GetAllWithDetails()
                .Select(x => x.Adapt<UserDto>());
            return result;
        }
        /// <summary>
        /// Method to get user by it's id.
        /// </summary>
        /// <param name="id">Unique identifier of the user to get.</param>
        /// <returns>User instance if found or null.</returns>
        public async Task<UserDto> GetByIdAsync(int id)
        {
            id.ValidateId();
            var user = await Database.UserRepository.GetByIdAsync(id);
            var result = user.Adapt<UserDto>();
            return result;
        }
        /// <summary>
        /// Updates user instance in db.
        /// </summary>
        /// <param name="dto">DTO to match real entity in db.</param>
        public void Update(UserDto dto)
        {
            dto.CheckEntity();
            dto.ValidateProperties();
            Database.UserRepository.Update(dto.Adapt<User>());
            Database.Save();
        }
    }
}
