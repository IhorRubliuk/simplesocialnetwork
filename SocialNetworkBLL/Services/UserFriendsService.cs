﻿using Mapster;
using SocialNetworkBLL.DTOs;
using SocialNetworkBLL.ExtensionMethods;
using SocialNetworkBLL.Interfaces;
using SocialNetworkDAL.Entities;
using SocialNetworkDAL.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialNetworkBLL.Services
{
    public class UserFriendsService : IUserFriendsService
    {
        /// <summary>
        /// Field for unit of work with access to contexts and repositories.
        /// </summary>
        private IUnitOfWork Database { get; set; }
        /// <summary>
        /// Constructor to init service with UoW.
        /// </summary>
        /// <param name="uow">Inistance of unit of work to initialize service with.</param>
        public UserFriendsService(IUnitOfWork uow)
        {
            Database = uow;
        }
        /// <summary>
        /// Method to add user friends to db.
        /// </summary>
        /// <param name="dto">DTO that contains data for the user friends to be added.</param>
        /// <returns>Task after code execution.</returns>
        public async Task AddAsync(UserFriendsDto dto)
        {
            dto.CheckEntity();
            dto.ValidateProperties();
            var entity = dto.Adapt<UserFriends>();
            await Database.UserFriendsRepository.AddAsync(entity);
            await Database.SaveAsync();
        }
        /// <summary>
        /// Method to remove entity from db.
        /// </summary>
        /// <param name="dto">DTO to match real entity in db.</param>
        public void Delete(UserFriendsDto dto)
        {
            dto.CheckEntity();
            dto.UserId.ValidateId();
            dto.FriendId.ValidateId();
            Database.UserFriendsRepository.Delete(dto.Adapt<UserFriends>());
            Database.Save();
        }
        /// <summary>
        /// Method to delete entity from db by it's id.
        /// </summary>
        /// <param name="id">Unique identifier of the entity to be deleted.</param>
        /// <returns>Task after code execution.</returns>
        public async Task DeleteByIdAsync(int id)
        {
            id.ValidateId();
            await Database.UserFriendsRepository.DeleteByIdAsync(id);
            await Database.SaveAsync();
        }
        /// <summary>
        /// Method to get all user friends from db.
        /// </summary>
        /// <returns>IEnumerable<UserFriendsDto> enumerable of user friends.</returns>
        public IEnumerable<UserFriendsDto> GetAll()
        {
            var result = Database.UserFriendsRepository.GetAll()
                .AsEnumerable()
                .Select(x => x.Adapt<UserFriendsDto>());
            return result;
        }
        /// <summary>
        /// Method to get all friends for a user.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IEnumerable<UserFriendsDto> GetAllFriendsForUser(int id)
        {
            var allUserFriends = GetAll();
            var result = allUserFriends.Where(x => x.UserId == id);
            return result;
        }
        /// <summary>
        /// Method to get user friends by it's id.
        /// </summary>
        /// <param name="id">Unique identifier of the user friends to get.</param>
        /// <returns>UserFriends instance if found or null.</returns>
        public async Task<UserFriendsDto> GetByIdAsync(int id)
        {
            id.ValidateId();
            var entity = await Database.UserFriendsRepository.GetByIdAsync(id);
            var result = entity.Adapt<UserFriendsDto>();
            return result;
        }
        /// <summary>
        /// Updates user friends instance in db.
        /// </summary>
        /// <param name="dto">DTO to match real entity in db.</param>
        public void Update(UserFriendsDto dto)
        {
            dto.CheckEntity();
            dto.ValidateProperties();
            Database.UserFriendsRepository.Update(dto.Adapt<UserFriends>());
            Database.Save();
        }

    }
}
