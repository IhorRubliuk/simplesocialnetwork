﻿using Mapster;
using SocialNetworkBLL.DTOs;
using SocialNetworkBLL.ExtensionMethods;
using SocialNetworkBLL.Interfaces;
using SocialNetworkDAL.Entities;
using SocialNetworkDAL.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialNetworkBLL.Services
{
    public class MessageService : IMessageService
    {
        /// <summary>
        /// Field for unit of work with access to contexts and repositories.
        /// </summary>
        private IUnitOfWork Database { get; set; }
        /// <summary>
        /// Constructor to init service with UoW.
        /// </summary>
        /// <param name="uow">Inistance of unit of work to initialize service with.</param>
        public MessageService(IUnitOfWork uow)
        {
            Database = uow;
        }
        /// <summary>
        /// Method to add message to db.
        /// </summary>
        /// <param name="dto">DTO that contains data for the message to be added.</param>
        /// <returns>Task after code execution.</returns>
        public async Task AddAsync(MessageDto dto)
        {
            dto.CheckEntity();
            dto.ValidateProperties();
            var entity = dto.Adapt<Message>();
            await Database.MessageRepository.AddAsync(entity);
            dto.Id = entity.Id;
            await Database.SaveAsync();
        }
        /// <summary>
        /// Method to remove entity from db.
        /// </summary>
        /// <param name="dto">DTO to match real entity in db.</param>
        public void Delete(MessageDto dto)
        {
            dto.CheckEntity();
            dto.Id.ValidateId();
            Database.MessageRepository.Delete(dto.Adapt<Message>());
            Database.Save();
        }
        /// <summary>
        /// Method to delete entity from db by it's id.
        /// </summary>
        /// <param name="id">Unique identifier of the entity to be deleted.</param>
        /// <returns>Task after code execution.</returns>
        public async Task DeleteByIdAsync(int id)
        {
            id.ValidateId();
            await Database.MessageRepository.DeleteByIdAsync(id);
            await Database.SaveAsync();
        }
        /// <summary>
        /// Method to get all messages from db.
        /// </summary>
        /// <returns>IEnumerable<MessageDto> enumerable of messages.</returns>
        public IEnumerable<MessageDto> GetAll()
        {
            var result = Database.MessageRepository.GetAll()
                .AsEnumerable()
                .Select(x => x.Adapt<MessageDto>());
            return result;
        }
        /// <summary>
        /// Method to get message by it's id.
        /// </summary>
        /// <param name="id">Unique identifier of the message to get.</param>
        /// <returns>Messages instance if found or null.</returns>
        public async Task<MessageDto> GetByIdAsync(int id)
        {
            id.ValidateId();
            var message = await Database.MessageRepository.GetByIdAsync(id);
            var result = message.Adapt<MessageDto>();
            return result;
        }
        /// <summary>
        /// Updates message instance in db.
        /// </summary>
        /// <param name="dto">DTO to match real entity in db.</param>
        public void Update(MessageDto dto)
        {
            dto.CheckEntity();
            dto.ValidateProperties();
            Database.MessageRepository.Update(dto.Adapt<Message>());
            Database.Save();
        }
    }
}
