﻿using Mapster;
using SocialNetworkBLL.DTOs;
using SocialNetworkBLL.ExtensionMethods;
using SocialNetworkBLL.Interfaces;
using SocialNetworkDAL.Entities;
using SocialNetworkDAL.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialNetworkBLL.Services
{
    public class RoleService : IRoleService
    {
        /// <summary>
        /// Field for unit of work with access to contexts and repositories.
        /// </summary>
        private IUnitOfWork Database { get; set; }
        /// <summary>
        /// Constructor to init service with UoW.
        /// </summary>
        /// <param name="uow">Inistance of unit of work to initialize service with.</param>
        public RoleService(IUnitOfWork uow) => Database = uow;
        /// <summary>
        /// Method to add user role to db.
        /// </summary>
        /// <param name="dto">DTO that contains data for the user role to be added.</param>
        /// <returns>Task after code execution.</returns>
        public async Task AddAsync(RoleDto dto)
        {
            dto.CheckEntity();
            dto.ValidateProperties();
            var entity = dto.Adapt<Role>();
            await Database.RoleRepository.AddAsync(entity);
            dto.Id = entity.Id;
            await Database.SaveAsync();
        }
        /// <summary>
        /// Method to remove entity from db.
        /// </summary>
        /// <param name="dto">DTO to match real entity in db.</param>
        public void Delete(RoleDto dto)
        {
            dto.CheckEntity();
            dto.Id.ValidateId();
            Database.RoleRepository.Delete(dto.Adapt<Role>());
            Database.Save();
        }
        /// <summary>
        /// Method to delete entity from db by it's id.
        /// </summary>
        /// <param name="id">Unique identifier of the entity to be deleted.</param>
        /// <returns>Task after code execution.</returns>
        public async Task DeleteByIdAsync(int id)
        {
            id.ValidateId();
            await Database.RoleRepository.DeleteByIdAsync(id);
            await Database.SaveAsync();
        }
        /// <summary>
        /// Method to get all roles from db.
        /// </summary>
        /// <returns>IEnumerable<RoleDto> enumerable of roles.</returns>
        public IEnumerable<RoleDto> GetAll()
        {
            var result = Database.RoleRepository.GetAll()
                .AsEnumerable()
                .Select(x => x.Adapt<RoleDto>());
            return result;
        }
        /// <summary>
        /// Method to get role by name;
        /// </summary>
        /// <param name="name">Name of the role to get.</param>
        /// <returns>Role found or null.</returns>
        public RoleDto GetRoleByName(string name)
        {
            name.ValidateString();
            var result = Database.RoleRepository.GetRoleByNameAsync(name).Result.Adapt<RoleDto>();
            return result;
        }
        /// <summary>
        /// Method to get user role by it's id.
        /// </summary>
        /// <param name="id">Unique identifier of the user role to get.</param>
        /// <returns>Role instance if found or null.</returns>
        public async Task<RoleDto> GetByIdAsync(int id)
        {
            id.ValidateId();
            var role = await Database.RoleRepository.GetByIdAsync(id);
            var result = role.Adapt<RoleDto>();
            return result;
        }
        /// <summary>
        /// Updates role instance in db.
        /// </summary>
        /// <param name="dto">DTO to match real entity in db.</param>
        public void Update(RoleDto dto)
        {
            dto.CheckEntity();
            dto.ValidateProperties();
            Database.RoleRepository.Update(dto.Adapt<Role>());
            Database.Save();
        }
    }
}
