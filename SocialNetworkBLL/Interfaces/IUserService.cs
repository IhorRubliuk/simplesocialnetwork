﻿using SocialNetworkBLL.DTOs;
using System.Collections.Generic;

namespace SocialNetworkBLL.Interfaces
{
    /// <summary>
    /// Interface for users.
    /// </summary>
    public interface IUserService : IService<UserDto> 
    {
        /// <summary>
        /// Method to get all users with all navigation properties included.
        /// </summary>
        /// <returns>IQueryable of users.</returns>
        IEnumerable<UserDto> GetAllWithDetails();
    }
}
