﻿using SocialNetworkBLL.DTOs;

namespace SocialNetworkBLL.Interfaces
{
    /// <summary>
    /// Service for picture repository access.
    /// </summary>
    public interface IPictureService : IService<PictureDto> { }
}
