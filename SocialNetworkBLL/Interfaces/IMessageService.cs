﻿using SocialNetworkBLL.DTOs;

namespace SocialNetworkBLL.Interfaces
{
    /// <summary>
    /// Service for message repository access.
    /// </summary>
    public interface IMessageService : IService<MessageDto> { }
}
