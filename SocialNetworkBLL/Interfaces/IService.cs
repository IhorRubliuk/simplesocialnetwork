﻿using SocialNetworkBLL.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SocialNetworkBLL.Interfaces
{
    public interface IService<TDto> where TDto : class
    {
        /// <summary>
        /// Method to get all entities from db.
        /// </summary>
        /// <returns>IEnumerable<Dto> from db.</returns>
        IEnumerable<TDto> GetAll();
        /// <summary>
        /// Method to get Dto by its id.
        /// </summary>
        /// <param name="id">Unique identifier of the Dto to get it by.</param>
        /// <returns>Instance of Dto type.</returns>
        Task<TDto> GetByIdAsync(int id);
        /// <summary>
        /// Method to add Dto to db.
        /// </summary>
        /// <param name="dto">Dto to add to database.</param>
        /// <returns>Task after code execution.</returns>
        Task AddAsync(TDto dto);
        /// <summary>
        /// Method to update dto in db.
        /// </summary>
        /// <param name="dto">Updated dto.</param>
        void Update(TDto dto);
        /// <summary>
        /// Method to delete dto from db.
        /// </summary>
        /// <param name="dto">dto to be deleted.</param>
        void Delete(TDto dto);
        /// <summary>
        /// Method to delete dto from db by id.
        /// </summary>
        /// <param name="id">Unique identifier of the dto to be deleted.</param>
        /// <returns>Task after code execution.</returns>
        Task DeleteByIdAsync(int id);
    }
}
