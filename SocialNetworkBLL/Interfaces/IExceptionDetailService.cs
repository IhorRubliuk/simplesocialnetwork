﻿using SocialNetworkBLL.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SocialNetworkBLL.Interfaces
{
    /// <summary>
    /// Basic service for all exceptiondetail entities.
    /// </summary>
    public interface IExceptionDetailService
    {
        /// <summary>
        /// Method to add exception detail to db.
        /// </summary>
        /// <param name="exceptionDetailDto">Dto to add to the db.</param>
        void Add(ExceptionDetailDto exceptionDetailDto);
        /// <summary>
        /// Method to remove exception details from db.
        /// </summary>
        /// <param name="exceptionDetailDto">Entity to remove from db.</param>
        void Remove(ExceptionDetailDto exceptionDetailDto);
        /// <summary>
        /// Method to get all logs.
        /// </summary>
        /// <returns>List of logs.</returns>
        Task<IEnumerable<ExceptionDetailDto>> GetAll();
    }
}
