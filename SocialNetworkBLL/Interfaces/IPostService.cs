﻿using SocialNetworkBLL.DTOs;

namespace SocialNetworkBLL.Interfaces
{
    /// <summary>
    /// Service for post repository access.
    /// </summary>
    public interface IPostService : IService<PostDto> { }
}
