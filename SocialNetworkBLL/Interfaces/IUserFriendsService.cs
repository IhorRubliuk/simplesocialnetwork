﻿using SocialNetworkBLL.DTOs;
using System.Collections.Generic;

namespace SocialNetworkBLL.Interfaces
{
    /// <summary>
    /// Basic interface for UserFriends service.
    /// </summary>
    public interface IUserFriendsService : IService<UserFriendsDto> 
    {
        /// <summary>
        /// Method to get all friends for a user.
        /// </summary>
        /// <param name="id">Unique identifier of the user we want to get friends of.</param>
        /// <returns>List of users if any. Null if none.</returns>
        IEnumerable<UserFriendsDto> GetAllFriendsForUser(int id);
    }
}
