﻿using SocialNetworkBLL.DTOs;

namespace SocialNetworkBLL.Interfaces
{
    /// <summary>
    /// Basic interface for user roles services.
    /// </summary>
    public interface IRoleService : IService<RoleDto> 
    {
        /// <summary>
        /// Method to get role by name;
        /// </summary>
        /// <param name="name">Name of the role to get.</param>
        /// <returns>Role found or null.</returns>
        public RoleDto GetRoleByName(string name);
    }
}
