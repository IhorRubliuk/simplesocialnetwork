﻿using Mapster;
using SocialNetworkBLL.DTOs;
using SocialNetworkDAL.Entities;

namespace SocialNetworkBLL.Configs
{
    /// <summary>
    /// Class for mapster registrations.
    /// </summary>
    class MapRegistration : IRegister
    {
        /// <summary>
        /// Method to register all types and interfaces.
        /// </summary>
        /// <param name="config">Instance of type adapter configuration where mapping will be added to.</param>
        public void Register(TypeAdapterConfig config)
        {
            config.NewConfig<ExceptionDetailDto, ExceptionDetail>()
                .Map(dest => dest.Id, src => src.Id)
                .Map(dest => dest.CreatedOn, src => src.CreatedOn);
            config.NewConfig<MessageDto, Message>()
                .Map(dest => dest.Id, src => src.Id)
                .Map(dest => dest.FromId, src => src.FromId)
                .Map(dest => dest.ToId, src => src.ToId)
                .Map(dest => dest.Text, src => src.Text)
                .Map(dest => dest.CreatedOn, src => src.CreatedOn);
            config.NewConfig<PictureDto, Picture>()
                .Map(dest => dest.Id, src => src.Id)
                .Map(dest => dest.Path, src => src.Path)
                .Map(dest => dest.CreatedOn, src => src.CreatedOn);
            config.NewConfig<PostDto, Post>()
                .Map(dest => dest.UserId, src => src.UserId)
                .Map(dest => dest.Text, src => src.Text)
                .Map(dest => dest.UserId, src => src.UserId)
                .Map(dest => dest.CreatedOn, src => src.CreatedOn);
            config.NewConfig<RoleDto, Role>()
                .Map(dest => dest.Id, src => src.Id)
                .Map(dest => dest.Name, src => src.Name);
            config.NewConfig<UserDto, User>()
                .Map(dest => dest.Id, src => src.Id)
                .Map(dest => dest.Name, src => src.Name)
                .Map(dest => dest.Email, src => src.Email)
                .Map(dest => dest.Password, src => src.Password)
                .Map(dest => dest.CreatedOn, src => src.CreatedOn)
                .Map(dest => dest.Posts, src => src.Posts)
                .Map(dest => dest.Friends, src => src.Friends)
                .Map(dest => dest.RoleId, src => src.RoleId)
                .Map(dest => dest.Role, src => src.Role);
            config.NewConfig<UserFriendsDto, UserFriends>()
                .Ignore(x => x.User)
                .Ignore(x => x.Friend)
                .Map(dest => dest.UserId, src => src.UserId)
                .Map(dest => dest.FriendId, src => src.FriendId);
        }
    }
}
