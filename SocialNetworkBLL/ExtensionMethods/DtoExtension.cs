﻿using SocialNetworkBLL.DTOs;
using SocialNetworkBLL.Validation;
using System;

namespace SocialNetworkBLL.ExtensionMethods
{
    /// <summary>
    /// Class with extension methods for BaseDto objects.
    /// </summary>
    internal static class DtoExtension
    {
        /// <summary>
        /// Method to check whether entity is null.
        /// </summary>
        /// <param name="dto">Entity to check.</param>
        public static void CheckEntity(this BaseDto dto)
        {
            if (dto is null) throw new SocialNetworkException("Cannot operate with dto of null.");
        }
        public static void ValidateProperties(this object dto)
        {
            foreach (var prop in dto.GetType().GetProperties())
            {
                if (prop != null)
                {
                    var type = prop?.PropertyType;
                    var value = dto.GetType().GetProperty(prop.Name)?.GetValue(dto);
                    if (type.Name is "String" && string.IsNullOrEmpty(value?.ToString()))
                    {
                        throw new SocialNetworkException($"{prop.Name} cannot be null or an empty string.");
                    }
                    if (type.Name is "DateTime" && value == default)
                    {
                        throw new SocialNetworkException($"{prop.Name} cannot be unset or default.");
                    }
                }
            }
        }

    }
}
