﻿using SocialNetworkBLL.Validation;

namespace SocialNetworkBLL.ExtensionMethods
{
    internal static class IntegerExtension
    {
        public static void ValidateId(this int id)
        {
            if (id == 0 || id < 0) throw new SocialNetworkException("Id cannot be equal to zero or less.");
        }
    }
}
