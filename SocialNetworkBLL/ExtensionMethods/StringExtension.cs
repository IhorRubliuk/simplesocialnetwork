﻿using SocialNetworkBLL.Validation;
using System;

namespace SocialNetworkBLL.ExtensionMethods
{
    /// <summary>
    /// Extension class for strings.
    /// </summary>
    internal static class StringExtension
    {
        /// <summary>
        /// Method to check if string provided is null or an empty string. If yes, SocialNetworkException is thrown.
        /// </summary>
        /// <param name="str">String to validate.</param>
        public static void ValidateString(this string str)
        {
            if (String.IsNullOrEmpty(str)) throw new SocialNetworkException("String provided was null or an empty string.");
        }
    }
}
