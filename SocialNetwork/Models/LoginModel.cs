﻿using System.ComponentModel.DataAnnotations;

namespace SocialNetwork.Models
{
    /// <summary>
    /// Model for user login.
    /// </summary>
    public class LoginModel
    {
        /// <summary>
        /// User's email.
        /// </summary>
        [Required(ErrorMessage = "Wrong email provided.")]
        public string Email { get; set; }
        /// <summary>
        /// User's password.
        /// </summary>
        [Required(ErrorMessage = "Не указан пароль")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
