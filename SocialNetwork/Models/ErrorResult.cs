﻿using System;
namespace SocialNetwork.Models
{
    /// <summary>
    /// Model for error result.
    /// </summary>
    public class ErrorResult
    {
        /// <summary>
        /// Type of the error.
        /// </summary>
        public string Type { get; set; }
        /// <summary>
        /// Message of the error.
        /// </summary>
        public string Message { get; set; }
        /// <summary>
        /// Stack trace of the error.
        /// </summary>
        public string StackTrace { get; set; }
        /// <summary>
        /// Constructor to create new instance of error result.
        /// </summary>
        /// <param name="ex">Instance of Exception, to get info about the error from.</param>
        public ErrorResult(Exception ex)
        {
            Type = ex.GetType().Name;
            Message = ex.Message;
            StackTrace = ex.ToString();
        }
    }
}
