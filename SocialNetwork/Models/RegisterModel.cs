﻿using System.ComponentModel.DataAnnotations;

namespace SocialNetwork.Models
{
    /// <summary>
    /// Model for user registration.
    /// </summary>
    public class RegisterModel
    {
        /// <summary>
        /// Email for the user.
        /// </summary>
        [Required(ErrorMessage = "Email was not provided.")]
        public string Email { get; set; }
        /// <summary>
        /// Name for the user.
        /// </summary>
        [Required(ErrorMessage = "Name was not provided.")]
        public string Name { get; set; }
        /// <summary>
        /// User's passsword.
        /// </summary>
        [Required(ErrorMessage = "Password was not provided.")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        /// <summary>
        /// User's password confirmation.
        /// </summary>
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Password confirmation is invalid.")]
        public string ConfirmPassword { get; set; }
    }
}
