﻿using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace SocialNetwork.Infrastructure
{
    /// <summary>
    /// Class for the authentication options.
    /// </summary>
    public static class AuthOptions
    {
        /// <summary>
        /// Token issuer.
        /// </summary>
        public const string ISSUER = "SNAuthServer"; 
        /// <summary>
        /// Token client.
        /// </summary>
        public const string AUDIENCE = "SNAuthClient";
        /// <summary>
        /// Key for hashing.
        /// </summary>
        const string KEY = "mysupersecret_secretkey!123";
        /// <summary>
        /// Token lifetime.
        /// </summary>
        public const int LIFETIME = 1;
        /// <summary>
        /// Method to get symetric key.
        /// </summary>
        /// <returns>Generated symetric key.</returns>
        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
        }
    }
}
