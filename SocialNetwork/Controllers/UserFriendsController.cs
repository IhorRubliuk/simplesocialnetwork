﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SocialNetworkBLL.DTOs;
using SocialNetworkBLL.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SocialNetwork.Controllers
{
    /// <summary>
    /// Controller to work with user friends.
    /// </summary>
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UserFriendsController : ControllerBase
    {
        /// <summary>
        /// Service to work with the picture repository.
        /// </summary>
        private readonly IUserFriendsService _userFriendsService;
        /// <summary>
        /// Controller to inject PictureService into controller.
        /// </summary>
        /// <param name="service"></param>
        public UserFriendsController(IUserFriendsService service) => _userFriendsService = service;
        /// <summary>
        /// Method to get all pictures.
        /// </summary>
        /// <returns>List of pictures..</returns>
        [HttpGet("{id}")]
        public ActionResult<IEnumerable<UserFriendsDto>> GetAllFriendsForUser(int id)
        {
            IEnumerable<UserFriendsDto> friends;
            try
            {
                friends = _userFriendsService.GetAllFriendsForUser(id);
            }
            catch
            {
                return BadRequest();
            }
            return Ok(friends);
        }
        /// <summary>
        /// Method to add new picture to db.  
        /// </summary>
        /// <param name="pictureDto">Entity with the picture path.</param>
        /// <returns>Ok 200 if created.</returns>
        [HttpPost]
        public async Task<ActionResult> Add([FromBody] UserFriendsDto userFriendsDto)
        {
            try
            {
                await _userFriendsService.AddAsync(userFriendsDto);
            }
            catch
            {
                return BadRequest();
            }
            return Ok();
        }
        /// <summary>
        /// Method to delete entity from db.
        /// </summary>
        /// <param name="id">Unique identifier of the entity to be deleted.</param>
        /// <returns>Returns Ok 200 result if successfull.</returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteById(int id)
        {
            try
            {
                await _userFriendsService.DeleteByIdAsync(id);
            }
            catch
            {
                return BadRequest();
            }
            return Ok();
        }
    }
}
