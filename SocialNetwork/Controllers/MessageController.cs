﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SocialNetworkBLL.DTOs;
using SocialNetworkBLL.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SocialNetwork.Controllers
{
    /// <summary>
    /// Controller for messages.
    /// </summary>
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class MessageController : ControllerBase
    {
        /// <summary>
        /// Service to work with the message repository.
        /// </summary>
        private readonly IMessageService _messageService;
        /// <summary>
        /// Controller to inject MessageService into controller.
        /// </summary>
        /// <param name="service"></param>
        public MessageController(IMessageService service) => _messageService = service;
        /// <summary>
        /// Method to get all messages.
        /// </summary>
        /// <returns>List of Messages.</returns>
        [HttpGet]
        public ActionResult<IEnumerable<MessageDto>> GetAllMessages()
        {
            IEnumerable<MessageDto> messages;
            try
            {
                messages = _messageService.GetAll();
            }
            catch
            {
                return BadRequest();
            }
            return Ok(messages);
        }
        /// <summary>
        /// Method to add message to db.
        /// </summary>
        /// <param name="messageDto">Message to add to db.</param>
        /// <returns>Ok 200 result if created.</returns>
        [HttpPost]
        public async Task<ActionResult> Add([FromBody] MessageDto messageDto)
        {
            try
            {
                await _messageService.AddAsync(messageDto);
            }
            catch
            {
                return BadRequest();
            }
            return Ok();
        }
        /// <summary>
        /// Method to get entity by it's id.
        /// </summary>
        /// <param name="id">Unique identifier of the corresponding entity.</param>
        /// <returns>Entity if found, empty arr if not.</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<UserDto>> GetById(int id)
        {
            MessageDto messageDto;
            try
            {
                messageDto = await _messageService.GetByIdAsync(id);
            }
            catch
            {
                return BadRequest();
            }
            return Ok(messageDto);
        }
        /// <summary>
        /// Method to update entity in db.
        /// </summary>
        /// <param name="messageDto">Message to update in db.</param>
        /// <returns>Updated message.</returns>
        [HttpPut]
        public async Task<ActionResult> Update([FromBody] MessageDto messageDto)
        {
            try
            {
                _messageService.Update(messageDto);
            }
            catch
            {
                return BadRequest();
            }
            return Ok(await _messageService.GetByIdAsync(messageDto.Id));
        }
        /// <summary>
        /// Method to delete entity from db.
        /// </summary>
        /// <param name="messageDto">Entity to remove from db.</param>
        /// <returns>200 Ok result if deleted.</returns>
        [HttpDelete]
        public ActionResult Delete([FromBody] MessageDto messageDto)
        {
            try
            {
                _messageService.Delete(messageDto);
            }
            catch
            {
                return BadRequest();
            }
            return Ok();
        }
        /// <summary>
        /// Method to delete entity from db.
        /// </summary>
        /// <param name="id">Unique identifier of the entity to be deleted.</param>
        /// <returns>Returns Ok 200 result if successfull.</returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteById(int id)
        {
            try
            {
                await _messageService.DeleteByIdAsync(id);
            }
            catch
            {
                return BadRequest();
            }
            return Ok();
        }
    }
}
