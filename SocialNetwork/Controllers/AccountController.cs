﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using SocialNetwork.Infrastructure;
using SocialNetwork.Models;
using SocialNetworkBLL.DTOs;
using SocialNetworkBLL.Interfaces;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace SocialNetwork.Controllers
{
    /// <summary>
    /// Controller for user accounts.
    /// </summary>
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        /// <summary>
        /// UserFriends service.
        /// </summary>
        private readonly IRoleService _roleService;
        /// <summary>
        /// User service.
        /// </summary>
        private readonly IUserService _userService;
        /// <summary>
        /// Constructor to initialize constructor with the services.
        /// </summary>
        /// <param name="userService">User service.</param>
        /// <param name="userFriendsService">UserFriends service.</param>
        public AccountController(IUserService userService, IRoleService roleService) =>
            (_userService, _roleService) = (userService, roleService);
        /// <summary>
        /// Method to get all users.
        /// </summary>
        /// <returns>All users from db.</returns>
        [HttpGet]
        public ActionResult<IEnumerable<UserDto>> GetAllUsers()
        {
            IEnumerable<UserDto> users;
            try
            {
                users = _userService.GetAll();
            }
            catch 
            {
                return BadRequest();
            }
            return Ok(users);
        }
        /// <summary>
        /// Method to add new user to db.
        /// </summary>
        /// <param name="model">Register model of the new user.</param>
        /// <returns>Returns new user created.</returns>
        [AllowAnonymous]
        [HttpPost]
        public async Task<ActionResult> Add([FromBody] RegisterModel model)
        {
            UserDto user;
            try
            {
                user = _userService.GetAll().FirstOrDefault(x => x.Email == model.Email);
                if (user is null)
                {
                    user = new UserDto()
                    {
                        Name = model.Name,
                        Email = model.Email,
                        Password = model.Password,
                        CreatedOn = DateTime.Now,
                        RoleId = _roleService.GetRoleByName("User").Id
                    };
                    await _userService.AddAsync(user);
                } 
                else return BadRequest();
            }
            catch
            {
                return BadRequest();
            }
            return CreatedAtAction(nameof(GetById), new { id = user.Id }, user);
        }
        /// <summary>
        /// Method to authenticate user.
        /// </summary>
        /// <param name="model">Model of the user with his username and password.</param>
        /// <returns>200 Ok if success. 400 BadRequest if failed.</returns>
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        [HttpPost("Authenticate")]
        public IActionResult Authenticate([FromBody]LoginModel model)
        {
            try
            {
                var user = _userService.GetAll().FirstOrDefault(x => x.Email == model.Email);
                if (user is null) return BadRequest();
                else if (user.Password != model.Password) return BadRequest();
                var identity = Authenticate(user).Result;
                var now = DateTime.UtcNow;
                var jwt = new JwtSecurityToken(
                        issuer: AuthOptions.ISSUER,
                        audience: AuthOptions.AUDIENCE,
                        notBefore: now,
                        claims: identity.Claims,
                        expires: now.Add(TimeSpan.FromMinutes(AuthOptions.LIFETIME)),
                        signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));
                var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

                var response = new
                {
                    access_token = encodedJwt,
                    username = identity.Name
                };
                return Ok(response);
            }
            catch
            {
                return BadRequest();
            }
        }
        /// <summary>
        /// Method to get entity by it's id.
        /// </summary>
        /// <param name="id">Unique identifier of the corresponding entity to get from db.</param>
        /// <returns>Entity if found. Empty array if not.</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<UserDto>> GetById(int id)
        {
            UserDto user;
            try
            {
                user = await _userService.GetByIdAsync(id);
            }
            catch
            {
                return BadRequest();
            }
            return Ok(user);
        }
        /// <summary>
        /// Method to get user by it's email.
        /// </summary>
        /// <param name="email">Email of the user we want to get id for.</param>
        /// <returns>User id of the user found or 0.</returns>
        [HttpGet("{email}")]
        public ActionResult<int> GetUserId(string email)
        {
            UserDto user;
            try
            {
                user = _userService.GetAll().FirstOrDefault(x => x.Email == email);
            }
            catch
            {
                return BadRequest();
            }
            return Ok(user?.Id);
        }
        /// <summary>
        /// Method to update user in db.
        /// </summary>
        /// <param name="user">User to be updated.</param>
        /// <returns>User updated.</returns>
        [HttpPut]
        public async Task<ActionResult> Update([FromBody] UserDto user)
        {
            try
            {
                _userService.Update(user);
            }
            catch
            {
                return BadRequest();
            }
            return Ok(await _userService.GetByIdAsync(user.Id));
        }
        /// <summary>
        /// Method to delete user from db.
        /// </summary>
        /// <param name="user">User to be deleted from db.</param>
        /// <returns>Results 200 Ok if deleted.</returns>
        [HttpDelete]
        public ActionResult Delete([FromBody] UserDto user)
        {
            try
            {
                _userService.Delete(user);
            }
            catch 
            {
                return BadRequest();
            }
            return Ok();
        }
        /// <summary>
        /// Method to delete entity from db.
        /// </summary>
        /// <param name="id">Unique identifier of the entity to be deleted.</param>
        /// <returns>Results 200 Ok if deleted.</returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteById(int id)
        {
            try
            {
                await _userService.DeleteByIdAsync(id);
            }
            catch
            {
                return BadRequest();
            }
            return Ok();
        }
        /// <summary>
        /// Method to authenticate user with claim.
        /// </summary>
        /// <param name="userName">User name to authenticate user.</param>
        /// <returns>Task after code execution.</returns>
        private async Task<ClaimsIdentity> Authenticate(UserDto user)
        {
            var claims = new List<Claim> {
                new Claim(ClaimsIdentity.DefaultNameClaimType, user.Email),
                new Claim(ClaimsIdentity.DefaultRoleClaimType, user.Role?.Name)
            };
            ClaimsIdentity claimsIdentity = new ClaimsIdentity(claims, "Token",
                ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            return claimsIdentity;
        }
    }
}
