﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SocialNetworkBLL.DTOs;
using SocialNetworkBLL.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SocialNetwork.Controllers
{
    /// <summary>
    /// Controller for posts.
    /// </summary>
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class PostController : ControllerBase
    {
        /// <summary>
        /// Service to work with the post repository.
        /// </summary>
        private readonly IPostService _postService;
        /// <summary>
        /// Controller to inject PostService into controller.
        /// </summary>
        /// <param name="service"></param>
        public PostController(IPostService service) => _postService = service;
        /// <summary>
        /// Method to get all posts.
        /// </summary>
        /// <returns>List of posts.</returns>
        [HttpGet]
        public ActionResult<IEnumerable<PostDto>> GetAllPosts()
        {
            IEnumerable<PostDto> posts;
            try
            {
                posts = _postService.GetAll();
            }
            catch
            {
                return BadRequest();
            }
            return Ok(posts);
        }
        /// <summary>
        /// Method to add new post to db.
        /// </summary>
        /// <param name="postDto">Post DTO to add to db.</param>
        /// <returns>Entity created.</returns>
        [HttpPost]
        public async Task<ActionResult> Add([FromBody] PostDto postDto)
        {
            try
            {
                await _postService.AddAsync(postDto);
            }
            catch
            {
                return BadRequest();
            }
            return CreatedAtAction(nameof(GetById), new { id = postDto.UserId }, postDto);
        }
        /// <summary>
        /// Method to get entity by it's id.
        /// </summary>
        /// <param name="id">UNique identifier of the corresponding entity.</param>
        /// <returns>Entity if found, empty arr if not.</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<UserDto>> GetById(int id)
        {
            PostDto post;
            try
            {
                post = await _postService.GetByIdAsync(id);
            }
            catch
            {
                return BadRequest();
            }
            return Ok(post);
        }
        /// <summary>
        /// Method to update post entity in db.
        /// </summary>
        /// <param name="postDto">Entity to update in db.</param>
        /// <returns>Entity updated.</returns>
        [HttpPut]
        public async Task<ActionResult> Update([FromBody] PostDto postDto)
        {
            try
            {
                _postService.Update(postDto);
            }
            catch
            {
                return BadRequest();
            }
            return Ok(await _postService.GetByIdAsync(postDto.UserId));
        }
        /// <summary>
        /// Method to delete provided entity.
        /// </summary>
        /// <param name="postDto">Entity to delete from db.</param>
        /// <returns>Returns Ok 200 result if successfull.</returns>
        [HttpDelete]
        public ActionResult Delete([FromBody] PostDto postDto)
        {
            try
            {
                _postService.Delete(postDto);
            }
            catch
            {
                return BadRequest();
            }
            return Ok();
        }
        /// <summary>
        /// Method to delete entity from db.
        /// </summary>
        /// <param name="id">Unique identifier of the entity to be deleted.</param>
        /// <returns>Returns Ok 200 result if successfull.</returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteById(int id)
        {
            try
            {
                await _postService.DeleteByIdAsync(id);
            }
            catch
            {
                return BadRequest();
            }
            return Ok();
        }
    }
}
