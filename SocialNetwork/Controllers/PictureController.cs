﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SocialNetworkBLL.DTOs;
using SocialNetworkBLL.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SocialNetwork.Controllers
{
    /// <summary>
    /// Controller for pictures.
    /// </summary>
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class PictureController : ControllerBase
    {
        /// <summary>
        /// Service to work with the picture repository.
        /// </summary>
        private readonly IPictureService _pictureService;
        /// <summary>
        /// Controller to inject PictureService into controller.
        /// </summary>
        /// <param name="service"></param>
        public PictureController(IPictureService service) => _pictureService = service;
        /// <summary>
        /// Method to get all pictures.
        /// </summary>
        /// <returns>List of pictures..</returns>
        [HttpGet]
        public ActionResult<IEnumerable<PictureDto>> GetAllPictures()
        {
            IEnumerable<PictureDto> picture;
            try
            {
                picture = _pictureService.GetAll();
            }
            catch
            {
                return BadRequest();
            }
            return Ok(picture);
        }
        /// <summary>
        /// Method to add new picture to db.
        /// </summary>
        /// <param name="pictureDto">Entity with the picture path.</param>
        /// <returns>Ok 200 if created.</returns>
        [HttpPost]
        public async Task<ActionResult> Add([FromBody] PictureDto pictureDto)
        {
            try
            {
                await _pictureService.AddAsync(pictureDto);
            }
            catch
            {
                return BadRequest();
            }
            return Ok();
        }
        /// <summary>
        /// Method to get entity by it's id.
        /// </summary>
        /// <param name="id">Unique identifier of the corresponding entity.</param>
        /// <returns>Entity if found, empty arr if not.</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<UserDto>> GetById(int id)
        {
            PictureDto pictureDto;
            try
            {
                pictureDto = await _pictureService.GetByIdAsync(id);
            }
            catch
            {
                return BadRequest();
            }
            return Ok(pictureDto);
        }
        /// <summary>
        /// Method to update entity in db.
        /// </summary>
        /// <param name="pictureDto">Entity to update.</param>
        /// <returns>Updated entity.</returns>
        [HttpPut]
        public async Task<ActionResult> Update([FromBody] PictureDto pictureDto)
        {
            try
            {
                _pictureService.Update(pictureDto);
            }
            catch
            {
                return BadRequest();
            }
            return Ok(await _pictureService.GetByIdAsync(pictureDto.Id));
        }
        /// <summary>
        /// Method to delete entity from db.
        /// </summary>
        /// <param name="pictureDto">Entity to delete.</param>
        /// <returns>200 Ok if deleted successfully.</returns>
        [HttpDelete]
        public ActionResult Delete([FromBody] PictureDto pictureDto)
        {
            try
            {
                _pictureService.Delete(pictureDto);
            }
            catch
            {
                return BadRequest();
            }
            return Ok();
        }
        /// <summary>
        /// Method to delete entity from db.
        /// </summary>
        /// <param name="id">Unique identifier of the entity to be deleted.</param>
        /// <returns>Returns Ok 200 result if successfull.</returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteById(int id)
        {
            try
            {
                await _pictureService.DeleteByIdAsync(id);
            }
            catch
            {
                return BadRequest();
            }
            return Ok();
        }
    }
}
