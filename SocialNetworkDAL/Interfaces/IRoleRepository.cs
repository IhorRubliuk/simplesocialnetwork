﻿using SocialNetworkDAL.Entities;
using System.Threading.Tasks;

namespace SocialNetworkDAL.Interfaces
{
    /// <summary>
    /// Basic inteface for user role.
    /// </summary>
    public interface IRoleRepository : IRepository<Role> 
    {
        /// <summary>
        /// Method to get role by name;
        /// </summary>
        /// <param name="name">Name of the role to get.</param>
        /// <returns>Role found or null.</returns>
        Task<Role> GetRoleByNameAsync(string name);
        /// <summary>
        /// Method to add user to role.
        /// </summary>
        /// <param name="userId">Id of the user to add to the role.</param>
        /// <param name="roleId">Id of the role add user to.</param>
        /// <returns>Task after code execution.</returns>
        Task AddUserToRole(int userId, int roleId);
    }
}
