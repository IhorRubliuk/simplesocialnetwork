﻿using SocialNetworkDAL.Entities;
using System.Collections.Generic;

namespace SocialNetworkDAL.Interfaces
{
    /// <summary>
    /// Interface for user repository.
    /// </summary>
    public interface IUserRepository : IRepository<User> 
    {
        /// <summary>
        /// Method to get all users with all navigation properties included.
        /// </summary>
        /// <returns>IQueryable of users.</returns>
        IEnumerable<User> GetAllWithDetails();
    }
}
