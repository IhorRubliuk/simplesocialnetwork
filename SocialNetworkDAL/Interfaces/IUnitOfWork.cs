﻿using System.Threading.Tasks;

namespace SocialNetworkDAL.Interfaces
{
    /// <summary>
    /// Interface for unit of work pattern to work with db.
    /// </summary>
    public interface IUnitOfWork
    {
        /// <summary>
        /// Repository for exception details.
        /// </summary>
        IExceptionDetailRepository ExceptionDetailRepository { get; }
        /// <summary>
        /// Repository for pictures.
        /// </summary>
        IPictureRepository PictureRepository { get; }
        /// <summary>
        /// Repository for messages.
        /// </summary>
        IMessageRepository MessageRepository { get; }
        /// <summary>
        /// Repository for posts.
        /// </summary>
        IPostRepository PostRepository { get; }
        /// <summary>
        /// Repository for user roles.
        /// </summary>
        IRoleRepository RoleRepository { get; }
        /// <summary>
        /// Repository for users.
        /// </summary>
        IUserRepository UserRepository { get; }
        /// <summary>
        /// Repository for user's friends.
        /// </summary>
        IUserFriendsRepository UserFriendsRepository { get; }
        /// <summary>
        /// Method to save changes.
        /// </summary>
        void Save();
        /// <summary>
        /// Method to save changes async.
        /// </summary>
        /// <returns>Task after code execution.</returns>
        Task<int> SaveAsync();
    }
}
