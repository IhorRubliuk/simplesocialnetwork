﻿using SocialNetworkDAL.Entities;

namespace SocialNetworkDAL.Interfaces
{
    public interface IUserFriendsRepository : IRepository<UserFriends> { }
}
