﻿using SocialNetworkDAL.Entities;

namespace SocialNetworkDAL.Interfaces
{
    /// <summary>
    /// Interface for picture repository.
    /// </summary>
    public interface IPictureRepository : IRepository<Picture> { }
}
