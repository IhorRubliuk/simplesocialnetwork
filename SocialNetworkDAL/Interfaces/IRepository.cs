﻿using System.Linq;
using System.Threading.Tasks;

namespace SocialNetworkDAL.Interfaces
{
    /// <summary>
    /// Basic interface for all repositories with the entities that inherit BaseEntity.
    /// </summary>
    /// <typeparam name="TEntity">Type of the entity that is derived from BaseEntity</typeparam>
    public interface IRepository<TEntity> where TEntity : class
    {
        /// <summary>
        /// Method to get all entities from db.
        /// </summary>
        /// <returns>IQueryable<Entity> from db.</returns>
        IQueryable<TEntity> GetAll();
        /// <summary>
        /// Method to get Entity by its id.
        /// </summary>
        /// <param name="id">Unique identifier of the entity to get it by.</param>
        /// <returns>Instance of Entity type.</returns>
        Task<TEntity> GetByIdAsync(int id);
        /// <summary>
        /// Method to add entity to db.
        /// </summary>
        /// <param name="entity">Entity to add to database.</param>
        /// <returns>Task after code execution.</returns>
        Task AddAsync(TEntity entity);
        /// <summary>
        /// Method to update entity in db.
        /// </summary>
        /// <param name="entity">Updated entity.</param>
        void Update(TEntity entity);
        /// <summary>
        /// Method to delete entity from db.
        /// </summary>
        /// <param name="entity">Entity to be deleted.</param>
        void Delete(TEntity entity);
        /// <summary>
        /// Method to delete entity from db by id.
        /// </summary>
        /// <param name="id">Unique identifier of the entity to be deleted.</param>
        /// <returns>Task after code execution.</returns>
        Task DeleteByIdAsync(int id);
    }
}
