﻿using SocialNetworkDAL.Entities;
using System;
using System.Collections.Generic;

namespace SocialNetworkDAL.Interfaces
{
    /// <summary>
    /// Basic interface for all exception detail repositories.
    /// </summary>
    public interface IExceptionDetailRepository : IDisposable
    {
        /// <summary>
        /// Add new entity to db.
        /// </summary>
        /// <param name="exceptionDetail">Entity to add to db.</param>
        void Add(ExceptionDetail exceptionDetail);
        /// <summary>
        /// Remove entity from db.
        /// </summary>
        /// <param name="exceptionDetail">Entity to remove from db.</param>
        void Remove(ExceptionDetail exceptionDetail);
        /// <summary>
        /// Method to get all logs.
        /// </summary>
        /// <returns>List of logs.</returns>
        IEnumerable<ExceptionDetail> GetAll();
    }
}
