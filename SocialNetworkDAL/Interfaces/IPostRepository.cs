﻿using SocialNetworkDAL.Entities;

namespace SocialNetworkDAL.Interfaces
{
    /// <summary>
    /// Basic interface for post repository.
    /// </summary>
    public interface IPostRepository : IRepository<Post> { }
}
