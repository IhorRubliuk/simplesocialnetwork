﻿using SocialNetworkDAL.Entities;

namespace SocialNetworkDAL.Interfaces
{
    /// <summary>
    /// Interface for message repository.
    /// </summary>
    public interface IMessageRepository : IRepository<Message> { }
}
