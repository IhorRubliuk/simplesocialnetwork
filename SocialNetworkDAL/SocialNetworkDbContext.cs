﻿using Microsoft.EntityFrameworkCore;
using SocialNetworkDAL.Entities;
using System;

namespace SocialNetworkDAL
{
    /// <summary>
    /// DbContext class for the application. 
    /// </summary>
    public class SocialNetworkDbContext : DbContext
    {
        /// <summary>
        /// Set with messages entities.
        /// </summary>
        public DbSet<Message> Messages { get; set; }
        /// <summary>
        /// Set with posts entites.
        /// </summary>
        public DbSet<Post> Posts { get; set; }
        /// <summary>
        /// Set with Pictures entities.
        /// </summary>
        public DbSet<Picture> Pictures { get; set; }
        /// <summary>
        /// Set with user role entities.
        /// </summary>
        public DbSet<Role> Roles { get; set; }
        /// <summary>
        /// Set with user entities.
        /// </summary>
        public DbSet<User> Users { get; set; }
        /// <summary>
        /// Set with UserFriends entities.
        /// </summary>
        public DbSet<UserFriends> UserFriends { get; set; }
        /// <summary>
        /// Constructor to initialize dbContext.
        /// </summary>
        /// <param name="options">DbContextOptions for for builder to initialize db context.</param>
        public SocialNetworkDbContext(DbContextOptions<SocialNetworkDbContext> options) : base(options) { }
        /// <summary>
        /// Method to add custom configurations and seed values on db creation.
        /// </summary>
        /// <param name="modelBuilder">Instance of context model builder.</param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var adminRole = new Role() { Id = 1, Name = "Admin" };
            var userRole = new Role() { Id = 2, Name = "User" };
            var admin = new User()
            {
                Id = 1,
                Name = "Super admin",
                Email = "admin@admin.com",
                Password = "p@ssw0rd",
                CreatedOn = DateTime.Now,
                RoleId = 1
            };
            var user1 = new User()
            {
                Id = 2,
                Name = "Simple user",
                Email = "su@user.com",
                Password = "p@ssw0rd",
                CreatedOn = DateTime.Now,
                RoleId = 2
            };
            var user2 = new User()
            {
                Id = 3,
                Name = "Simple user 1",
                Email = "su1@user.com",
                Password = "p@ssw0rd",
                CreatedOn = DateTime.Now,
                RoleId = 2
            };
            var user3 = new User()
            {
                Id = 4,
                Name = "Simple user 2",
                Email = "su2@user.com",
                Password = "p@ssw0rd",
                CreatedOn = DateTime.Now,
                RoleId = 2
            };

            modelBuilder.Entity<Role>().HasData(new Role[] { adminRole, userRole });
            modelBuilder.Entity<User>().HasData(new User[] { admin, user1, user2, user3 });
            modelBuilder.Entity<UserFriends>()
                .HasOne(x => x.Friend)
                .WithMany()
                .HasForeignKey(x => x.FriendId)
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<UserFriends>()
                .HasOne(x => x.User)
                .WithMany(x => x.Friends)
                .HasForeignKey(x => x.UserId);
        }
    }
}
