﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SocialNetworkDAL.Migrations
{
    public partial class SocialNetworkDALSocialNetworkDbContextSeed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Roles",
                columns: new[] { "Id", "Name" },
                values: new object[] { 1, "Admin" });

            migrationBuilder.InsertData(
                table: "Roles",
                columns: new[] { "Id", "Name" },
                values: new object[] { 2, "User" });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "CreatedOn", "Email", "Name", "Password", "RoleId" },
                values: new object[,]
                {
                    { 2, new DateTime(2020, 11, 25, 15, 48, 5, 398, DateTimeKind.Local).AddTicks(7328), "su@user.com", "Simple user", "p@ssw0rd", 1 },
                    { 3, new DateTime(2020, 11, 25, 15, 48, 5, 398, DateTimeKind.Local).AddTicks(7384), "su1@user.com", "Simple user 1", "p@ssw0rd", 1 },
                    { 4, new DateTime(2020, 11, 25, 15, 48, 5, 398, DateTimeKind.Local).AddTicks(7389), "su2@user.com", "Simple user 2", "p@ssw0rd", 1 },
                    { 1, new DateTime(2020, 11, 25, 15, 48, 5, 395, DateTimeKind.Local).AddTicks(9703), "admin@admin.com", "Super admin", "p@ssw0rd", 2 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: 2);
        }
    }
}
