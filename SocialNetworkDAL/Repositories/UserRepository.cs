﻿using Microsoft.EntityFrameworkCore;
using SocialNetworkDAL.Entities;
using SocialNetworkDAL.ExtensionMethods;
using SocialNetworkDAL.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SocialNetworkDAL.Repositories
{
    /// <summary>
    /// Repository to work with the User entities.
    /// </summary>
    public class UserRepository : IUserRepository
    {
        /// <summary>
        /// Instance of db context.
        /// </summary>
        internal readonly SocialNetworkDbContext snDbContext;
        /// <summary>
        /// User db set from db context.
        /// </summary>
        internal readonly DbSet<User> dbSet;
        /// <summary>
        /// Constructor to initialize repository with db context.
        /// </summary>
        /// <param name="context">Instance of db context.</param>
        public UserRepository(SocialNetworkDbContext context)
        {
            snDbContext = context;
            dbSet = context.Set<User>();
        }
        /// <summary>
        /// Method to add entities to db.
        /// </summary>
        /// <param name="entity">Entity to add to db.</param>
        /// <returns>Task after code execution.</returns>
        public async Task AddAsync(User entity)
        {
            entity.CheckEntity();
            await dbSet.AddAsync(entity);
        }
        /// <summary>
        /// Method to delete entity from db.
        /// </summary>
        /// <param name="entity"></param>
        public void Delete(User entity)
        {
            entity.CheckEntity();
            if (snDbContext.Entry(entity).State == EntityState.Detached)
            {
                dbSet.Attach(entity);
            }
            dbSet.Remove(entity);
        }
        /// <summary>
        /// Method to delete entity from db by id.
        /// </summary>
        /// <param name="id">Unique identifier to correspond unique entity.</param>
        /// <returns></returns>
        public async Task DeleteByIdAsync(int id)
        {
            id.CheckId();
            var entity = dbSet.FindAsync(id);
            Delete(await entity);
        }
        /// <summary>
        /// Method to get all entities from db.
        /// </summary>
        /// <returns>IQueryable<User> from db.</returns>
        public IQueryable<User> GetAll()
        {
            var result = dbSet.Include(x => x.Role);
            return result;
        }
        /// <summary>
        /// Method to get all users with all navigation properties included.
        /// </summary>
        /// <returns>IQueryable of users.</returns>
        public IEnumerable<User> GetAllWithDetails()
        {
            var result = dbSet.Include(x => x.Posts).Include(x => x.Friends).Include(x => x.Role).AsEnumerable();
            return result;
        }
        /// <summary>
        /// Method to get entity from db by id.
        /// </summary>
        /// <param name="id">Unique identifier to correspond unique entity.</param>
        /// <returns>Task<User> with the users found. Null if no result found in db.</returns>
        public async Task<User> GetByIdAsync(int id)
        {
            id.CheckId();
            var result = await dbSet.FindAsync(id);
            return result;
        }
        /// <summary>
        /// Method to update entity in db.
        /// </summary>
        /// <param name="entity">Entity to update in db.</param>
        public void Update(User entity)
        {
            entity.CheckEntity();
            if (snDbContext.Entry(entity).State == EntityState.Detached)
            {
                dbSet.Attach(entity);
            }
            snDbContext.Entry(entity).State = EntityState.Modified;
        }
    }
}
