﻿using Microsoft.EntityFrameworkCore;
using SocialNetworkDAL.Entities;
using SocialNetworkDAL.ExtensionMethods;
using SocialNetworkDAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SocialNetworkDAL.Repositories
{
    public class ExceptionDetailRepository : IExceptionDetailRepository
    {
        /// <summary>
        /// Db context for our application.
        /// </summary>
        internal readonly SocialNetworkDbContext database;
        /// <summary>
        /// Db set for ExceptionDetail entities.
        /// </summary>
        internal readonly DbSet<ExceptionDetail> dbSet;
        /// <summary>
        /// Constructor to initialize repository with db context.
        /// </summary>
        /// <param name="context">Instance of db context to intialize repository with.</param>
        public ExceptionDetailRepository(SocialNetworkDbContext context)
        {
            database = context;
            dbSet = context.Set<ExceptionDetail>();
        }
        /// <summary>
        /// Add new entity to db.
        /// </summary>
        /// <param name="exceptionDetail">Entity to add to db.</param>
        public void Add(ExceptionDetail exceptionDetail)
        {
            exceptionDetail.CheckEntity();
            dbSet.Add(exceptionDetail);
        }
        /// <summary>
        /// Remove entity from db.
        /// </summary>
        /// <param name="exceptionDetail">Entity to remove from db.</param>
        public void Remove(ExceptionDetail exceptionDetail)
        {
            exceptionDetail.CheckEntity();
            dbSet.Remove(exceptionDetail);
        }
        /// <summary>
        /// Method to get all logs.
        /// </summary>
        /// <returns>List of logs.</returns>
        public IEnumerable<ExceptionDetail> GetAll()
        {
            var result = dbSet.AsEnumerable();
            return result;
        }

        #region IDisposable Support
        private bool _disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed && disposing)
            {
                database.Dispose();
            }
            _disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
