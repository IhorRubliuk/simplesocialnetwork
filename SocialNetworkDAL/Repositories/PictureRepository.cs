﻿using Microsoft.EntityFrameworkCore;
using SocialNetworkDAL.Entities;
using SocialNetworkDAL.ExtensionMethods;
using SocialNetworkDAL.Interfaces;
using System.Linq;
using System.Threading.Tasks;

namespace SocialNetworkDAL.Repositories
{
    /// <summary>
    /// Repository to work with the picture entities.
    /// </summary>
    public class PictureRepository : IPictureRepository
    {
        /// <summary>
        /// Instance of db context.
        /// </summary>
        internal readonly SocialNetworkDbContext snDbContext;
        /// <summary>
        /// Picture db set from db context.
        /// </summary>
        internal readonly DbSet<Picture> dbSet;
        /// <summary>
        /// Constructor to initialize repository with db context.
        /// </summary>
        /// <param name="context">Instance of db context.</param>
        public PictureRepository(SocialNetworkDbContext context)
        {
            snDbContext = context;
            dbSet = context.Set<Picture>();
        }
        /// <summary>
        /// Method to add entities to db.
        /// </summary>
        /// <param name="entity">Entity to add to db.</param>
        /// <returns>Task after code execution.</returns>
        public async Task AddAsync(Picture entity)
        {
            entity.CheckEntity();
            await dbSet.AddAsync(entity);
        }
        /// <summary>
        /// Method to delete entity from db.
        /// </summary>
        /// <param name="entity"></param>
        public void Delete(Picture entity)
        {
            entity.CheckEntity();
            if (snDbContext.Entry(entity).State == EntityState.Detached)
            {
                dbSet.Attach(entity);
            }
            dbSet.Remove(entity);
        }
        /// <summary>
        /// Method to delete entity from db by id.
        /// </summary>
        /// <param name="id">Unique identifier to correspond unique entity.</param>
        /// <returns></returns>
        public async Task DeleteByIdAsync(int id)
        {
            id.CheckId();
            var entity = dbSet.FindAsync(id);
            Delete(await entity);
        }
        /// <summary>
        /// Method to get all entities from db.
        /// </summary>
        /// <returns>IQueryable<Picture> from db.</returns>
        public IQueryable<Picture> GetAll()
        {
            var result = dbSet;
            return result;
        }
        /// <summary>
        /// Method to get entity from db by id.
        /// </summary>
        /// <param name="id">Unique identifier to correspond unique entity.</param>
        /// <returns>Task<Picture> with the picture found. Null if no result found in db.</returns>
        public async Task<Picture> GetByIdAsync(int id)
        {
            id.CheckId();
            var result = await dbSet.FindAsync(id);
            return result;
        }
        /// <summary>
        /// Method to update entity in db.
        /// </summary>
        /// <param name="entity">Entity to update in db.</param>
        public void Update(Picture entity)
        {
            entity.CheckEntity();
            if (snDbContext.Entry(entity).State == EntityState.Detached)
            {
                dbSet.Attach(entity);
            }
            snDbContext.Entry(entity).State = EntityState.Modified;
        }
    }
}
