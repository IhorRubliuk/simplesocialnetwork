﻿using Microsoft.EntityFrameworkCore;
using SocialNetworkDAL.Entities;
using SocialNetworkDAL.ExtensionMethods;
using SocialNetworkDAL.Interfaces;
using System.Linq;
using System.Threading.Tasks;

namespace SocialNetworkDAL.Repositories
{
    public class RoleRepository : IRoleRepository
    {
        /// <summary>
        /// Instance of db context.
        /// </summary>
        internal readonly SocialNetworkDbContext snDbContext;
        /// <summary>
        /// Role db set from db context.
        /// </summary>
        internal readonly DbSet<Role> dbSet;
        /// <summary>
        /// Constructor to initialize repository with db context.
        /// </summary>
        /// <param name="context">Instance of db context.</param>
        public RoleRepository(SocialNetworkDbContext context)
        {
            snDbContext = context;
            dbSet = context.Set<Role>();
        }
        /// <summary>
        /// Method to add entities to db.
        /// </summary>
        /// <param name="entity">Entity to add to db.</param>
        /// <returns>Task after code execution.</returns>
        public async Task AddAsync(Role entity)
        {
            entity.CheckEntity();
            await dbSet.AddAsync(entity);
        }
        /// <summary>
        /// Method to add user to role.
        /// </summary>
        /// <param name="userId">Id of the user to add to the role.</param>
        /// <param name="roleId">Id of the role add user to.</param>
        /// <returns>Task after code execution.</returns>
        public async Task AddUserToRole(int userId, int roleId)
        {
            var user = await snDbContext.Users.FindAsync(userId);
            user.CheckEntity();
            var role = await dbSet.FindAsync(roleId);
            role.Users.Add(user);
            Update(role);

        }
        /// <summary>
        /// Method to delete entity from db.
        /// </summary>
        /// <param name="entity"></param>
        public void Delete(Role entity)
        {
            entity.CheckEntity();
            if (snDbContext.Entry(entity).State == EntityState.Detached)
            {
                dbSet.Attach(entity);
            }
            dbSet.Remove(entity);
        }
        /// <summary>
        /// Method to delete entity from db by id.
        /// </summary>
        /// <param name="id">Unique identifier to correspond unique entity.</param>
        /// <returns></returns>
        public async Task DeleteByIdAsync(int id)
        {
            id.CheckId();
            var entity = dbSet.FindAsync(id);
            Delete(await entity);
        }
        /// <summary>
        /// Method to get all entities from db.
        /// </summary>
        /// <returns>IQueryable<Role> from db.</returns>
        public IQueryable<Role> GetAll()
        {
            var result = dbSet;
            return result;
        }
        /// <summary>
        /// Method to get role by name;
        /// </summary>
        /// <param name="name">Name of the role to get.</param>
        /// <returns>Role found or null.</returns>
        public async Task<Role> GetRoleByNameAsync(string name)
        {
            var result = await dbSet.FirstOrDefaultAsync(x => x.Name == name);
            return result;
        }
        /// <summary>
        /// Method to get entity from db by id.
        /// </summary>
        /// <param name="id">Unique identifier to correspond unique entity.</param>
        /// <returns>Task<Role> with the roles found. Null if no result found in db.</returns>
        public async Task<Role> GetByIdAsync(int id)
        {
            id.CheckId();
            var result = await dbSet.FindAsync(id);
            return result;
        }
        /// <summary>
        /// Method to update entity in db.
        /// </summary>
        /// <param name="entity">Entity to update in db.</param>
        public void Update(Role entity)
        {
            entity.CheckEntity();
            if (snDbContext.Entry(entity).State == EntityState.Detached)
            {
                dbSet.Attach(entity);
            }
            snDbContext.Entry(entity).State = EntityState.Modified;
        }
    }
}
