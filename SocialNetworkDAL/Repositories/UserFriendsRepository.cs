﻿using Microsoft.EntityFrameworkCore;
using SocialNetworkDAL.Entities;
using SocialNetworkDAL.ExtensionMethods;
using SocialNetworkDAL.Interfaces;
using System.Linq;
using System.Threading.Tasks;

namespace SocialNetworkDAL.Repositories
{
    /// <summary>
    /// Repository to interact with the user friends entities.
    /// </summary>
    class UserFriendsRepository : IUserFriendsRepository
    {
        /// <summary>
        /// Instance of db context.
        /// </summary>
        internal readonly SocialNetworkDbContext snDbContext;
        /// <summary>
        /// UserFriends db set from db context.
        /// </summary>
        internal readonly DbSet<UserFriends> dbSet;
        /// <summary>
        /// Constructor to initialize repository with db context.
        /// </summary>
        /// <param name="context">Instance of db context.</param>
        public UserFriendsRepository(SocialNetworkDbContext context)
        {
            snDbContext = context;
            dbSet = context.Set<UserFriends>();
        }
        /// <summary>
        /// Method to add entities to db.
        /// </summary>
        /// <param name="entity">Entity to add to db.</param>
        /// <returns>Task after code execution.</returns>
        public async Task AddAsync(UserFriends entity)
        {
            entity.CheckEntity();
            await dbSet.AddAsync(entity);
        }
        /// <summary>
        /// Method to delete entity from db.
        /// </summary>
        /// <param name="entity"></param>
        public void Delete(UserFriends entity)
        {
            entity.CheckEntity();
            if (snDbContext.Entry(entity).State == EntityState.Detached)
            {
                dbSet.Attach(entity);
            }
            dbSet.Remove(entity);
        }
        /// <summary>
        /// Method to delete entity from db by id.
        /// </summary>
        /// <param name="id">Unique identifier to correspond unique entity.</param>
        /// <returns></returns>
        public async Task DeleteByIdAsync(int id)
        {
            id.CheckId();
            var entity = dbSet.FindAsync(id);
            Delete(await entity);
        }
        /// <summary>
        /// Method to get all entities from db.
        /// </summary>
        /// <returns>IQueryable<UserFriends> from db.</returns>
        public IQueryable<UserFriends> GetAll()
        {
            var result = dbSet;
            return result;
        }
        /// <summary>
        /// Method to get entity from db by id.
        /// </summary>
        /// <param name="id">Unique identifier to correspond unique entity.</param>
        /// <returns>Task<UserFriends> with the friends if the user found. Null if no result found in db.</returns>
        public async Task<UserFriends> GetByIdAsync(int id)
        {
            id.CheckId();
            var result = await dbSet.FindAsync(id);
            return result;
        }
        /// <summary>
        /// Method to update entity in db.
        /// </summary>
        /// <param name="entity">Entity to update in db.</param>
        public void Update(UserFriends entity)
        {
            entity.CheckEntity();
            if (snDbContext.Entry(entity).State == EntityState.Detached)
            {
                dbSet.Attach(entity);
            }
            snDbContext.Entry(entity).State = EntityState.Modified;
        }
    }
}
