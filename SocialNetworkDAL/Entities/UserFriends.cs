﻿using SocialNetworkDAL.Entities.Base;

namespace SocialNetworkDAL.Entities
{
    /// <summary>
    /// Many-to-many for user and his friends.
    /// </summary>
    public class UserFriends: BaseEntity
    {
        /// <summary>
        /// Id of the user whos friends are.
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// Navigation property for user whom friends belong to.
        /// </summary>
        public virtual User User { get; set; }
        /// <summary>
        /// Id of the user's friend.
        /// </summary>
        public int FriendId { get; set; }
        /// <summary>
        /// Navigation property for friend.
        /// </summary>
        public virtual User Friend { get; set; }
    }
}
