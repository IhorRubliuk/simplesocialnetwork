﻿using SocialNetworkDAL.Entities.Base;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace SocialNetworkDAL.Entities
{
    /// <summary>
    /// Class for user posts.
    /// </summary>
    public class Post : BaseEntity
    {
        /// <summary>
        /// Id of the user post belongs to.
        /// </summary>
        [ForeignKey("User")]
        public int UserId { get; set; }
        /// <summary>
        /// Text of the post.
        /// </summary>
        public string Text { get; set; }
        /// <summary>
        /// Date and time created.
        /// </summary>
        public DateTime CreatedOn { get; set; }
        /// <summary>
        /// Navigational property for user.
        /// </summary>
        public virtual User User { get; set; }
    }
}
