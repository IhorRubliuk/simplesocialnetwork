﻿using SocialNetworkDAL.Entities.Base;
using System;

namespace SocialNetworkDAL.Entities
{
    /// <summary>
    /// Class to manage user messages.
    /// </summary>
    public class Message : BaseEntity
    {
        /// <summary>
        /// Id of the user message wa sent from.
        /// </summary>
        public int FromId { get; set; }
        /// <summary>
        /// Id of the user message was sent to.
        /// </summary>
        public int ToId { get; set; }
        /// <summary>
        /// Text message that was sent.
        /// </summary>
        public string Text { get; set; }
        /// <summary>
        /// Date and time created.
        /// </summary>
        public DateTime CreatedOn { get; set; }
    }
}
