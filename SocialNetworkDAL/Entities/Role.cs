﻿using SocialNetworkDAL.Entities.Base;
using System.Collections.Generic;

namespace SocialNetworkDAL.Entities
{
    /// <summary>
    /// Entity for role.
    /// </summary>
    public class Role : BaseEntity
    {
        /// <summary>
        /// Name of the role.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Navigation property for users.
        /// </summary>
        public ICollection<User> Users { get; set; }
    }
}
