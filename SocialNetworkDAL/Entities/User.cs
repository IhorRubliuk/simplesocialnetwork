﻿using SocialNetworkDAL.Entities.Base;
using System;
using System.Collections.Generic;

namespace SocialNetworkDAL.Entities
{
    /// <summary>
    /// Entity for the user.
    /// </summary>
    public class User : BaseEntity
    {
        /// <summary>
        /// User name.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// User email.
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// User password.
        /// </summary>
        public string Password { get; set; }
        /// <summary>
        /// Date and time created.
        /// </summary>
        public DateTime CreatedOn { get; set; }
        /// <summary>
        /// Navigation property for friends. Many-to-many to itself db relationship.
        /// </summary>
        public virtual ICollection<UserFriends> Friends { get; set; }
        /// <summary>
        /// Navigation property for posts. One-to-many relationships.
        /// </summary>
        public virtual ICollection<Post> Posts { get; set; }
        /// <summary>
        /// Foreign key for role.
        /// </summary>
        public int RoleId { get; set; }
        /// <summary>
        /// Navigation property for user role.
        /// </summary>
        public virtual Role Role { get; set; }

    }
}
