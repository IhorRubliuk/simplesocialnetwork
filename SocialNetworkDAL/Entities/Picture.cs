﻿using SocialNetworkDAL.Entities.Base;
using System;

namespace SocialNetworkDAL.Entities
{
    /// <summary>
    /// Class for pictures in the system.
    /// </summary>
    public class Picture : BaseEntity
    {
        /// <summary>
        /// Path to the picture stored in file system.
        /// </summary>
        public string Path { get; set; }
        /// <summary>
        /// Date and time created.
        /// </summary>
        public DateTime CreatedOn { get; set; }
    }
}
