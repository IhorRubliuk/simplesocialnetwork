﻿using System.ComponentModel.DataAnnotations;

namespace SocialNetworkDAL.Entities.Base
{
    /// <summary>
    /// Base entity for all entites that would have an Id.
    /// </summary>
    public class BaseEntity
    {
        /// <summary>
        /// Unique identifier for the entity
        /// </summary>
        [Key]
        public int Id { get; set; }
    }
}
