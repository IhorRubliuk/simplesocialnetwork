﻿using SocialNetworkDAL.Entities.Base;
using System;

namespace SocialNetworkDAL.Entities
{
    /// <summary>
    /// Exception details for the logging purposes.
    /// </summary>
    public class ExceptionDetail : BaseEntity
    {
        /// <summary>
        /// Message of the exception
        /// </summary>
        public string ExceptionMessage { get; set; }
        /// <summary>
        /// Name of the controller where exception occurred
        /// </summary>
        public string ControllerName { get; set; }
        /// <summary>
        /// Name of the action where exception occurred.
        /// </summary>
        public string ActionName { get; set; }
        /// <summary>
        /// Stack trace of the exception.
        /// </summary>
        public string StackTrace { get; set; }
        /// <summary>
        /// Date and time of the exception.
        /// </summary>
        public DateTime CreatedOn { get; set; }
    }
}
