﻿using System;

namespace SocialNetworkDAL.ExtensionMethods
{
    /// <summary>
    /// Extension method to check BaseEntities.
    /// </summary>
    public static class ObjectExtension
    {
        /// <summary>
        /// Method to check whether entity is null.
        /// </summary>
        /// <param name="entity">Entity to check.</param>
        public static void CheckEntity(this object entity)
        {
            if (entity is null) throw new ArgumentNullException("entity", "Cannot operate with argument of null");
        }
    }
}
