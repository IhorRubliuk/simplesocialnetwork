﻿using System;

namespace SocialNetworkDAL.ExtensionMethods
{
    /// <summary>
    /// Integers extension class.
    /// </summary>
    public static class IntegerExtension
    {
        /// <summary>
        /// Method to check whether id is comlient to all id requirements.
        /// </summary>
        /// <param name="integer">Id to check.</param>
        public static void CheckId(this int integer)
        {
            if (integer < 0) throw new ArgumentException("Id cannot be less than 0.", "integer");
        }
    }
}
