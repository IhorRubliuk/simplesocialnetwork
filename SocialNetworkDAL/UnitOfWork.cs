﻿using SocialNetworkDAL.Interfaces;
using SocialNetworkDAL.Repositories;
using System.Threading.Tasks;

namespace SocialNetworkDAL
{
    /// <summary>
    /// Unit of work to interact with db context.
    /// </summary>
    public class UnitOfWork : IUnitOfWork
    {
        /// <summary>
        /// Db context. Only to read.
        /// </summary>
        private readonly SocialNetworkDbContext _context;
        /// <summary>
        /// Repository to work with the exception details entities.
        /// </summary>
        public IExceptionDetailRepository ExceptionDetailRepository
        {
            get
            {
                return new ExceptionDetailRepository(_context);
            }
        }
        /// <summary>
        /// Message repository to work with message entities.
        /// </summary>
        public IMessageRepository MessageRepository 
        {
            get
            {
                return new MessageRepository(_context);
            }
        }
        /// <summary>
        /// Picture repository.
        /// </summary>
        public IPictureRepository PictureRepository
        {
            get
            {
                return new PictureRepository(_context);
            }
        }
        /// <summary>
        /// Post repository to work with post entities.
        /// </summary>
        public IPostRepository PostRepository
        {
            get
            {
                return new PostRepository(_context);
            }
        }
        /// <summary>
        /// Role repository to work with user role entities.
        /// </summary>
        public IRoleRepository RoleRepository
        {
            get 
            {
                return new RoleRepository(_context);
            }
        }
        /// <summary>
        /// User repository to access user entities.
        /// </summary>
        public IUserRepository UserRepository
        {
            get
            {
                return new UserRepository(_context);
            }
        }
        /// <summary>
        /// UserFriends repository to access user friends entities.
        /// </summary>
        public IUserFriendsRepository UserFriendsRepository
        {
            get 
            {
                return new UserFriendsRepository(_context);
            }
        }
        /// <summary>
        /// Constructor to initialize unit of work with the context.
        /// </summary>
        /// <param name="context"></param>
        public UnitOfWork(SocialNetworkDbContext context) => _context = context;
        /// <summary>
        /// Method to save changes done to dbContext.
        /// </summary>
        public void Save() => _context.SaveChanges();
        /// <summary>
        /// Method to save changes done to dbContext asynchronously.
        /// </summary>
        /// <returns>Task integer after code execution.</returns>
        public async Task<int> SaveAsync() => await _context.SaveChangesAsync();
    }
}
